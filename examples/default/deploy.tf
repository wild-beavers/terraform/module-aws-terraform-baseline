locals {
  prefix = "${random_string.start_letter.result}${random_string.this.result}"
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

module "default" {
  source = "../../"

  prefix = local.prefix

  iam_policy_terraform_protection_enable = true
  iam_policy_terraform_protection_name   = "tftest"
}

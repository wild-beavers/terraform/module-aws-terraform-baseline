#####
# Terraform protection policy
#####

output "iam_policy_terraform_protection_id" {
  value = try(aws_iam_policy.terraform_protection["0"].id, null)
}

output "iam_policy_terraform_protection_arn" {
  value = try(aws_iam_policy.terraform_protection["0"].arn, null)
}

output "iam_policy_policy_terraform_policy_id" {
  value = try(aws_iam_policy.terraform_protection["0"].policy_id, null)
}

output "iam_policy_terraform_protection_path" {
  value = try(aws_iam_policy.terraform_protection["0"].path, null)
}

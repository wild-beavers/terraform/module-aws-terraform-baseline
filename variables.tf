#####
# General
#####

variable "prefix" {
  type        = string
  description = "Prefix to be used for all resources names. Specifically useful for tests."
  default     = ""
}

variable "tags" {
  type        = map(string)
  description = "Tags to be used in every resources created by this module."
  default     = null
}

#####
# Terraform protection policy
#####

variable "iam_policy_terraform_protection_enable" {
  type        = bool
  description = "Whether ot not to create the IAM policy to protect all resources tagged `managed-by`=`terraform` of any write operation."
  default     = false
}

variable "iam_policy_terraform_protection_name" {
  type        = string
  description = "Name of the policy to create for protecting all resources tagged `managed-by`=`terraform`. Ignored if `var.iam_policy_terraform_protection_enable` is `false`."
  default     = "TerraformResourcesProtection"

  validation {
    condition     = can(regex("^[_+=,\\.@a-zA-Z0-9-]{1,128}$", var.iam_policy_terraform_protection_name))
    error_message = "The var.iam_policy_name must match “^[_+=,\\.@a-zA-Z0-9-]{1,128}$”."
  }
}

## 1.0.5

- maintenance: migrate module to Terraform registry
- chore: bump pre-commit hooks

# 1.0.4

- fix: missing `elasticfilesystem:Client*` IAM actions, and remove firehose for actions size constraint
- chore: bump pre-commit hooks
- test: make test comply to new standards

# 1.0.3

- maintenance: remove `aws-portal:View*` and add new IAM actions

# 1.0.2

- chore: updates pre-commit dependencies
- fix: allows Terraform protection policy to perform `kms:Generate*` actions, as it is a write data action
- fix: renames output `iam_policy_policy_terraform_protection_id` to `iam_policy_policy_terraform_policy_id`

# 1.0.1

- feat: removes README from the example
- maintenance: fix TFlint issues
- refactor: use for_each instead of count
- test: adds gitlab ci
- chore: bump pre-commit hooks

# 1.0.0

- feat: allow resource tag `alterable-by = user` to alter Terraform managed resources
- feat: (BREAKING) removes `elasticbeanstalk` service to decrease policy size limit to allow alterable tags condition
- chore: update pre-commit schema
- chore: bump pre-commit hooks
- doc: add policy limitations

# 0.0.4

- fix: allows Terraform protection policy to perform basic IAM actions

# 0.0.3

- fix: allows Terraform protection policy to kms:decrypt to read data. Protecting KMS and data should be done elsewhere.
- refactor: use `Desc*` instead of `Describe*` to push further the character limits per policy on terraform protection

# 0.0.2

- fix: allows Terraform protection policy to putObject & deleteObject in S3

# 0.0.1

- fix: makes sure Terraform protection policy still allows to switch role
- refactor: make the `var.tags` merged with local tags in `locals` to prevent mistakes

# 0.0.0

- Initial version

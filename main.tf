locals {
  tags = merge(
    {
      managed-by = "terraform"
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-terraform-baseline"
    },
    var.tags
  )
}

#####
# Terraform protection policy
#####

resource "aws_iam_policy" "terraform_protection" {
  for_each = var.iam_policy_terraform_protection_enable ? { 0 = 0 } : {}

  name   = format("%s%s", var.prefix, var.iam_policy_terraform_protection_name)
  path   = "/"
  policy = data.aws_iam_policy_document.terraform_protection["0"].json
  tags   = local.tags
}
